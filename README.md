
P8 Jonathan de la osa: TOURGUIDE<br/>
MicroService: Tourguide <br/>
## *Architecture *
Patterns-microservices-springcloud <br/>
Spring boot gradle <br/>
Git hub <br/><br/><br/>
<img src = "https://drive.google.com/uc?id=1qV5HLh9FyhQsAi1b4tGomMFkSuxKPNwQ" title = "google logo" alt = "Google logo">
*********************
##*Repository:*
TourGuide:<br/>https://gitlab.com/jonathan_fullstack/p8-jd.git <br/>
TourGuideServer: <br/>https://gitlab.com/jonathan_fullstack/tourduideServer.git <br/>
GpsUtilsMicroService: <br/>https://gitlab.com/jonathan_fullstack/tourguide-gpsUtil.git <br/>
RewardMicroService: <br/>https://gitlab.com/jonathan_fullstack/tourguide-reward.git<br/>
TripMasterMicroService:<br/> https://gitlab.com/jonathan_fullstack/tourguide-trippricer.git <br/>
*********************
## *Commande principale*
Build: Gradlew  build <br/>
Test: gradlew test <br/>
Run: Gradlew bootRun <br/>
*********************
##*Micro-service:*
TourGuide: Port 8085 <br/>
TourGuideServer: Port 8086 <br/>
GpsUtilsMicroService: Port 8081 <br/>
RewardMicroService: Port: 8082 <br/>
TripMasterMicroService: Port 8083 <br/>
*********************
##*Docker command:*
docker build -t "MicroService"+:VERSION <br/>
docker run -p "port" + "MicroService" or <br/>
docker-compose up
*********************
